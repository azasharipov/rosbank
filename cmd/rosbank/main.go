package main

import (
	"database/sql"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
	"rosbank/pkg/handlers"
	"rosbank/pkg/items"
	"rosbank/pkg/middleware"
	"rosbank/pkg/session"
	"rosbank/pkg/user"
)

func main() {
	templates := template.Must(template.ParseFiles("./template/base.html"))

	connStr := "user=aza password=mypass dbname=rosbank sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync() // flushes buffer, if any
	logger := zapLogger.Sugar()

	servicesRepo := items.NewServicesRepo(db)
	userRepo := user.NewUserRepo()
	sm := session.NewSessionsMem()
	userHandler := &handlers.UserHandler{
		Tmpl:     templates,
		UserRepo: userRepo,
		Logger:   logger,
		Sessions: sm,
	}
	handlers := &handlers.ServicesHandlers{
		Tmpl:         templates,
		Logger:       logger,
		ServicesRepo: servicesRepo,
	}
	//dir := "./template/static/"
	r := mux.NewRouter()
	r.HandleFunc("/", userHandler.Index).Methods("GET")
	r.HandleFunc("/services", handlers.GetServices).Methods("GET")
	//r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	mux := middleware.Auth(sm, r)
	mux = middleware.AccessLog(logger, mux)
	mux = middleware.Panic(mux)

	addr := ":8080"
	logger.Infow("starting server",
		"type", "START",
		"addr", addr,
	)
	http.ListenAndServe(addr, mux)
}
