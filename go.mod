module rosbank

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.2.0
	go.uber.org/zap v1.13.0
)
