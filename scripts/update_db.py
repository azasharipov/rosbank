#!/usr/bin/python3

import psycopg2
import sys
from filelock import FileLock
from options import connection_conf, main_table, query_path


def update_tab(cursor):
    fcntl.flock(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    with open(query_path, "r") as inp:
        cmd = inp.read()
    fcntl.flock(file, fcntl.LOCK_UN)
    cursor.execute(cmd)
    fcntl.flock(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    with open(query_path, "w") as inp:
        pass
    fcntl.flock(file, fcntl.LOCK_UN)


def main():
    conn = psycopg2.connect(**connection_conf)
    cursor = conn.cursor()
    update_tab(cursor)
    conn.commit()
    conn.close()


if __name__ == "__main__":
    main()
