#!/usr/bin/python3

import psycopg2
import os
from options import connection_conf, docs_conf,\
    query_path, main_table, help_table


def init_database(cursor):
    sql_create_main_table =\
    """CREATE TABLE IF NOT EXISTS {} (\
    id INTEGER PRIMARY KEY, {})""".format(
        main_table,
        ", ".join([
            "doc_{}_{} VARCHAR(10) DEFAULT 'push'".format(i, j)
            for i in range(1, docs_conf["num_uslug"] + 1)
            for j in range(1, docs_conf["docs_per_u"] + 1)
        ])
    )

    sql_create_help_table =\
    """CREATE TABLE IF NOT EXISTS {} (
    id INTEGER PRIMARY KEY,
    role INTEGER DEFAULT 4)
    """.format(
        help_table
    )

    cursor.execute(sql_create_main_table)
    cursor.execute(sql_create_help_table)


def main():
    conn = psycopg2.connect(**connection_conf)
    cursor = conn.cursor()
    init_database(cursor)
    conn.commit()
    conn.close()
    os.system("touch {}".format(query_path))


if __name__ == "__main__":
    main()
