#!/usr/bin/python3

import sys
import fcntl
from time import sleep
from options import main_table, query_path

def create_query(id, *args):
    sql_query = "UPDATE {} SET {} WHERE id = {}\n".format(
        main_table,
        ",".join(
            ["{}={}".format(key, val)
            for key, val in zip(args[::2], args[1::2])]),
        id)
    with open(query_path, "a") as file:
        fcntl.flock(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        file.write(sql_query)
        fcntl.flock(file, fcntl.LOCK_UN)


def main():
    create_query(*sys.argv[1:])


if __name__ == "__main__":
    main()
