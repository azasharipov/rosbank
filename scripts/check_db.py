#!/usr/bin/python3

import sys
import fcntl
from options import query_path, docs_conf


def check_file(id_req):
    col_dict = {}
    with open(query_path, "r") as file:
        fcntl.flock(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        for line in file:
            parsed = line.split()
            set, id = parsed[3], parsed[-1]
            if id != id_req:
                continue
            col_dict = { eq.split("=")[0] : eq.split("=")[1]
                for eq in set.split(",") }
        fcntl.flock(file, fcntl.LOCK_UN)
    print(col_dict)


def main():
    check_file(sys.argv[1])


if __name__ == "__main__":
    main()
