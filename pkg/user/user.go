package user

type Chanel string

type Documents [][]Chanel

type User struct {
	ID          uint32
	LegalEntity string
	Config      Documents
}

type UserRepo struct {
	data map[uint32]*User
}

func NewUserRepo() *UserRepo {
	return &UserRepo{
		data: map[uint32]*User{
			1: &User{
				ID:          1,
				LegalEntity: "abc",
				Config:      [][]Chanel{{"email"}},
			},
		},
	}
}

func FindById(id uint32, users []User) *User {
	for _, u := range users {
		if u.ID == id {
			return &u
		}
	}
	return nil
}
