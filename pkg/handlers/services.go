package handlers

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os/exec"
	"rosbank/pkg/items"
	"rosbank/pkg/user"
	"sync"

	"go.uber.org/zap"
)

type ServicesHandlers struct {
	Tmpl         *template.Template
	ServicesRepo *items.ServicesRepository
	Logger       *zap.SugaredLogger
	UserRepo     *user.UserRepo
}

func (h *ServicesHandlers) GetServices(w http.ResponseWriter, r *http.Request) {
	var wg sync.WaitGroup
	wg.Add(1)
	fmt.Print("starting")
	go func() {
		cmd := exec.Command("/usr/bin/python3", "scripts/check_db.py", "1")

		jsfile := new(bytes.Buffer)
		err2 := new(bytes.Buffer)
		cmd.Stdout = jsfile
		cmd.Stderr = err2
		err := cmd.Run()
		fmt.Printf(err2.String())
		if err != nil {
			fmt.Errorf("error")
			log.Fatal(err)
		}
		fmt.Printf(jsfile.String())

		wg.Done()
	}()
	wg.Wait()
}

//func (h *ServicesHandlers) (w http.ResponseWriter, r *http.Request) {
//}
